#!/usr/bin/env bash

sudo apt-get update

# instala o suporte a NFS (sem ele o Vagrant fica uma carroça)
# Notas:
# - para usarmos Vagrant com NFS é necessário que tanto host quanto guest tenham suporte instalado.
# - no host, rode: sudo apt-get install nfs-common nfs-kernel-server portmap (note o nfs-common nfs-kernel-server que o guest não tem)
# - embora tenha colocado o pacote 'portmap', o apt-get instalou o pacote 'rpcbind' em seu lugar.
sudo apt-get install nfs-common portmap -y

sudo apt-get install -y php5 build-essential g++ git-core \
apache2 \
php5-xdebug \
php-apc \
php5-curl \
php5-gd \
php5-imagick \
php5-mssql \
php-pear \
php5-cli \
php5-json \
php5-mcrypt \
php5-intl \
php-apc \
php5-memcached  \
php5-memcache \
memcached \
php5-dev \
libyaml-dev \
php5-mysql \
php5-intl \
php5-imap

sudo a2enmod rewrite

# Configura xdebug
cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.scream=0
xdebug.cli_color=1
xdebug.show_local_vars=1
xdebug.max_nesting_level=250
EOF

sudo service apache2 restart
